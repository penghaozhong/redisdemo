package com.phz.redis.redisdeo.dao;

import com.phz.redis.redisdeo.entity.Product;
import com.phz.redis.redisdeo.rpc.PayDepositProductFacadeClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

/**
 * @Auther: penghaozhong
 * @Date: 2018/9/29 14:38
 */
@Component
public class ProductDao {

//    @Autowired
//    private PayDepositProductFacadeClient client;

    @Cacheable(value = "phz_test_product1",key = "'getList'")
    public List<Product> getList(){
        System.out.println("==============进查询了");
        List<Product> list = new ArrayList<>();
        IntStream.range(1,10).forEach(i->{
            Product p = new Product();
            p.setProductCode("code"+i);
            p.setFromDate("2018"+i);
            p.setProductName("华悦财富宝00"+i+"期");
            p.setExecuteRate("5.6");
            p.setPappAmt("1000"+i);
            list.add(p);
        });

        return list;
    }

//,condition = "#result.productCode eq '#p.productCode'"
    @CacheEvict(value = "phz_test_product1",key = "'getList'")
    public void cancelByCode(Product p ){
        System.out.println(p.getProductCode() + " 产品更新了");
    }


    // ,key = "'productDao:getListById:' + #id"
    @Cacheable(cacheNames = "phz_test_product1",key = "'getListById:' + #id")
    public List<Product> getListById(String id){

        List<Product> list = new ArrayList<>();
        IntStream.range(1,10).filter(i->{
            return i == Integer.parseInt(id);
        }).forEach(i->{
            Product p = new Product();
            p.setFromDate("20190417");
            p.setProductName("华悦财富宝00"+i+"期");
            p.setExecuteRate("5.6");
            p.setPappAmt("1000"+i);
            list.add(p);
        });

        return list;
    }
}
