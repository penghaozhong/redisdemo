package com.phz.redis.redisdeo.entity;


import java.io.Serializable;

/**
 * @Auther: penghaozhong
 * @Date: 2018/9/29 15:42
 */

public class Product  implements Serializable {


    private  String productCode;
    private  String fromDate;
    private  String productName;
    private  String executeRate;
    private  String pappAmt;

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getExecuteRate() {
        return executeRate;
    }

    public void setExecuteRate(String executeRate) {
        this.executeRate = executeRate;
    }

    public String getPappAmt() {
        return pappAmt;
    }

    public void setPappAmt(String pappAmt) {
        this.pappAmt = pappAmt;
    }


    @Override
    public String toString() {
        return "Product{" +
                "productCode='" + productCode + '\'' +
                ", fromDate='" + fromDate + '\'' +
                ", productName='" + productName + '\'' +
                ", executeRate='" + executeRate + '\'' +
                ", pappAmt='" + pappAmt + '\'' +
                '}';
    }
}
