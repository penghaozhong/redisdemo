package com.phz.redis.redisdeo.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.phz.redis.redisdeo.dao.ProductDao;
import com.phz.redis.redisdeo.entity.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * @Auther: penghaozhong
 * @Date: 2018/9/29 14:35
 */
@Service
public class ProductService {

    public static final String KEY = "phz_test_list";
    public static final String Map = "phz_test_map";

    ObjectMapper mapper = new ObjectMapper();

    @Autowired
    private ProductDao dao;

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    public List<Product> getList(){
       return dao.getList();
    }


    public List<Product> getListById(String id){
        return dao.getListById(id);
    }


    public void addList(){

        List<Product> list = IntStream.range(1, 10).mapToObj(i -> {
            Product p = new Product();
            p.setFromDate("20190417");
            p.setProductName("华悦财富宝00" + i + "期");
            p.setExecuteRate("5.6");
            p.setPappAmt("1000" + i);


            try {
                stringRedisTemplate.opsForList().rightPush("user:001",mapper.writeValueAsString(p));
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }

            return p;

        }).collect(Collectors.toList());


//        String json = null;
//        try {
//             json = mapper.writeValueAsString(list);
//        } catch (JsonProcessingException e) {
//            e.printStackTrace();
//        }
//        System.out.println(json);

        List range = redisTemplate.opsForList().range(KEY, 0, -1);
        try {
            System.out.println("======从redis中读取的数据");
            System.out.println(mapper.writeValueAsString(range));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

    }

    public void addMap(){
        ObjectMapper mapper = new ObjectMapper();

        IntStream.range(100, 105).forEach((int u) -> {
            List<Product> list = IntStream.range(1, 5).mapToObj(i -> {
                Product p = new Product();
                p.setProductCode("" + (u + i));
                p.setFromDate("20190417");
                p.setProductName("华悦财富宝00" + (u + i) + "期");
                p.setExecuteRate("5.6");
                p.setPappAmt("1000" + (u + i));
                return p;
            }).collect(Collectors.toList());

            try {
                System.out.println(mapper.writeValueAsString(list));
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }

            redisTemplate.opsForHash().put(Map,u,list);
            System.out.println(redisTemplate.opsForHash().get(Map, u));
        });




//        String json = null;
//        try {
//             json = mapper.writeValueAsString(list);
//        } catch (JsonProcessingException e) {
//            e.printStackTrace();
//        }
//        System.out.println(json);

        System.out.println("======从redis中读取的数据");

        java.util.Map<Integer,List<Product>> entries  = redisTemplate.opsForHash().entries(Map);
        entries.forEach((k,v)->{
            System.out.println(v);
            System.out.println(v.toString());
        });

//        Set keys = redisTemplate.opsForHash().keys(Map);
//        keys.forEach(key->{
//            System.out.println(key.toString());
//
//            java.util.Map entries = redisTemplate.opsForHash().entries(key);
//
//            redisTemplate.opsForHash().values(key).forEach(v->{
//               try {
//                   System.out.println(mapper.writeValueAsString(v));
//               } catch (JsonProcessingException e) {
//                   e.printStackTrace();
//               }
//           });
//        });


    }

    public  void simpleMap(){
        Product p = new Product();
        p.setProductCode("001");
        p.setFromDate("20190417");
        p.setProductName("华悦财富宝00001期");
        p.setExecuteRate("5.6");
        p.setPappAmt("1000");
        try {
            stringRedisTemplate.opsForHash().put("sMap",p.getProductCode(), mapper.writeValueAsString(p));

            System.out.println(stringRedisTemplate.opsForHash().get("sMap", p.getProductCode()));

        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    public  void cancel(String code){
        Product p = new Product();
        p.setProductCode("code5");
        p.setFromDate("2018055");
        p.setProductName("华悦财富宝0055期");
        p.setExecuteRate("5.6");
        p.setPappAmt("100055");
        dao.cancelByCode(p);
    }


}
