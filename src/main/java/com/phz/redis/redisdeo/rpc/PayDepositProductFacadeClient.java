package com.phz.redis.redisdeo.rpc;

import org.springframework.cloud.netflix.feign.FeignClient;

/**
 * @author:chenglong
 * @dateTime:2018/7/31 14:16
 */
@FeignClient(value = "weilai-pay-server",url = "http://192.16.0.96:18888")
public interface PayDepositProductFacadeClient{
}
