package com.phz.redis.redisdeo.cache;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.cache.Cache;
import org.springframework.cache.transaction.AbstractTransactionSupportingCacheManager;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.cache.DefaultRedisCachePrefix;
import org.springframework.data.redis.cache.RedisCache;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.cache.RedisCachePrefix;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisOperations;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;

import java.util.*;

public class RedisExtCacheManager extends AbstractTransactionSupportingCacheManager {

    private final RedisOperations redisOperations;
    private boolean loadRemoteCachesOnStartup = false;
    private Set<String> configuredCacheNames =  Collections.<String> emptySet();
    private long defaultExpiration = 0;
    private Map<String, Long> expires = null;
    private boolean usePrefix = true;
    private boolean dynamic = true;

    private RedisCachePrefix cachePrefix = new DefaultRedisCachePrefix();

    private final boolean cacheNullValues;

    private final Log logger = LogFactory.getLog(RedisCacheManager.class);

    public RedisExtCacheManager(RedisOperations redisOperations) {
        this.redisOperations = redisOperations;
        this.cacheNullValues = false;
    }
    @Override
    protected Collection<? extends Cache> loadCaches() {
        Assert.notNull(this.redisOperations, "A redis template is required in order to interact with data store");

        Set<Cache> caches = new LinkedHashSet<Cache>( new ArrayList<Cache>());

        Set<String> cachesToLoad = new LinkedHashSet<String>(this.configuredCacheNames);
        cachesToLoad.addAll(this.getCacheNames());

        if (!CollectionUtils.isEmpty(cachesToLoad)) {

            for (String cacheName : cachesToLoad) {
                caches.add(createCache(cacheName));
            }
        }

        return caches;
    }

    @Override
    protected Cache getMissingCache(String name) {
        return this.dynamic ? createCache(name) : null;
    }

    protected RedisTempleExtCache createCache(String cacheName) {
        long expiration = computeExpiration(cacheName);
        return new RedisTempleExtCache(cacheName, (usePrefix ? cachePrefix.prefix(cacheName) : null), redisOperations, expiration,
                cacheNullValues);
    }

    protected long computeExpiration(String name) {
        Long expiration = null;
        if (expires != null) {
            expiration = expires.get(name);
        }
        return (expiration != null ? expiration.longValue() : defaultExpiration);
    }

    protected List<Cache> loadAndInitRemoteCaches() {

        List<Cache> caches = new ArrayList<Cache>();

        try {
            Set<String> cacheNames = loadRemoteCacheKeys();
            if (!CollectionUtils.isEmpty(cacheNames)) {
                for (String cacheName : cacheNames) {
                    if (null == super.getCache(cacheName)) {
                        caches.add(createCache(cacheName));
                    }
                }
            }
        } catch (Exception e) {
            if (logger.isWarnEnabled()) {
                logger.warn("Failed to initialize cache with remote cache keys.", e);
            }
        }

        return caches;
    }

    protected Set<String> loadRemoteCacheKeys() {
        return (Set<String>) redisOperations.execute(new RedisCallback<Set<String>>() {

            @Override
            public Set<String> doInRedis(RedisConnection connection) throws DataAccessException {

                // we are using the ~keys postfix as defined in RedisCache#setName
                Set<byte[]> keys = connection.keys(redisOperations.getKeySerializer().serialize("*~keys"));
                Set<String> cacheKeys = new LinkedHashSet<String>();

                if (!CollectionUtils.isEmpty(keys)) {
                    for (byte[] key : keys) {
                        cacheKeys.add(redisOperations.getKeySerializer().deserialize(key).toString().replace("~keys", ""));
                    }
                }

                return cacheKeys;
            }
        });
    }
}
