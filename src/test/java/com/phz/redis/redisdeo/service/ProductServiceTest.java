package com.phz.redis.redisdeo.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.phz.redis.redisdeo.entity.Product;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.core.BoundHashOperations;
import org.springframework.data.redis.core.BoundZSetOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.Assert.*;

/**
 * @Auther: penghaozhong
 * @Date: 2018/9/29 15:05
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductServiceTest {
    // 理财模块：用户id：起息中
    // 理财模块：用户id：逾期中
    public static final String MAP_KEY_QIXI = "product:003:201";
    public static final String MAP_KEY_YQ = "product:003:203";
    public static final String ZSET_KEY = "user:product:002";
    @Autowired
    private ProductService service;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private RedisTemplate redisTemplate;

    ObjectMapper mapper = new ObjectMapper();

    @Test
    public void getList() {
        List<Product> list = service.getList();
        list.forEach(System.out::println);

        System.out.println("第二次查询==========");
        service.getList();
        list.forEach(System.out::println);
    }

    @Test
    public void getListById() {
        List<Product> list = service.getListById("2");
        list.forEach(System.out::println);
        Assert.assertNotNull(list);
    }


    @Test
    public void cancelByCode(){
        service.cancel("code5");

    }

    @Test
    public void deleteByCode(){

        Product p = new Product();
        p.setProductCode("code5");
        p.setFromDate("20185");
        p.setProductName("华悦财富宝005期");
        p.setExecuteRate("5.6");
        p.setPappAmt("10005");
        redisTemplate.opsForZSet().remove("phz_test_product1:getList",p);
    }


    @Test
    public void addList() {
       service.addList();
    }

    @Test
    public void testList() {

        List<String> stringList = stringRedisTemplate.opsForList().range("user:001", 0, -1);
        stringList.forEach(p->{
            Product product = null;
            try {
                product = mapper.readValue(p, Product.class);
                System.out.println(product);
            } catch (IOException e) {
                e.printStackTrace();
            }

        });

    }

    @Test
    public void zsetAddTest() {
        IntStream.range(1, 10).mapToObj(i -> {
            Product p = new Product();
            p.setFromDate("20190417");
            p.setProductName("华悦财富宝00" + i + "期");
            p.setExecuteRate("5.6");
            p.setPappAmt("1000" + i);


            try {
                Boolean sucess = stringRedisTemplate.opsForZSet().add(ZSET_KEY, mapper.writeValueAsString(p), Integer.parseInt(p.getPappAmt()));
                Assert.assertTrue(sucess);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            return p;

        }).collect(Collectors.toList());
    }
    @Test
    public void zsetGetTest() {
        Set<String> set = stringRedisTemplate.boundZSetOps(ZSET_KEY).range(0,-1);
        set.forEach(p->{
            Product product = null;
            try {
                product = mapper.readValue(p, Product.class);
                System.out.println(product);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    @Test
    public void zsetUpdateTest() {
        BoundZSetOperations<String, String> zSetOps = stringRedisTemplate.boundZSetOps(ZSET_KEY);

    }

    @Test
    public void mapAdddTest() {

        BoundHashOperations<String, Object, Object> hashOps = stringRedisTemplate.boundHashOps(MAP_KEY_QIXI);
        IntStream.range(1, 10).forEach(i-> {
            Product p = new Product();
            p.setProductCode("id"+i);
            p.setFromDate("20190417");
            p.setProductName("华悦财富宝00" + i + "期");
            p.setExecuteRate("5.6");
            p.setPappAmt("1000" + i);
            try {
                hashOps.put(p.getProductCode(),mapper.writeValueAsString(p));
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        });
        }

    @Test
    public void mapGetTest() {

        BoundHashOperations<String, Object, Object> hashOps = stringRedisTemplate.boundHashOps(MAP_KEY_QIXI);
        hashOps.entries().forEach((k,v)->{
            Product product = null;
            try {
                product = mapper.readValue(v.toString(), Product.class);
                System.out.println(product);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        Object o = hashOps.get("id9");
        try {
            Product product = null;
            System.out.println("==========");
            product = mapper.readValue(o.toString(), Product.class);
            System.out.println(product);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Product p1 = new Product();
        p1.setProductCode("id"+9);
        p1.setFromDate("201819");
        p1.setProductName("华悦财富宝00" + 19 + "期");
        p1.setExecuteRate("1.9");
        p1.setPappAmt("1000" + 19);

        try {
            hashOps.put("id9",mapper.writeValueAsString(p1));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

       @Test
    public void mapTest() {
        service.addMap();
    }

    @Test
    public void simplemapTest() {
        service.simpleMap();
    }


    Jackson2JsonRedisSerializer<Object> jackson2JsonRedisSerializer = new Jackson2JsonRedisSerializer<Object>(
            Object.class);



}